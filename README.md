# 开源不易，感谢支持

 ![支付宝微信](doc/zfbwx.png)  

## 如需购买、定制流式细胞仪控件、软件，可联系微信：Lvwl-CN 或邮箱： <Lvwl@outlook.com> ，说明来意，会及时回复

## FlowCytometerControls 流式细胞专用图表控件,包含FCS文件读写

 支持.NET8、.NET6、.NETFramework 4.6.2及以上的WPF  
 支持FCS3.2、FCS3.1、FCS3.0、FCS2.0标准文件协议  
 支持千万级数据显示（电脑内存要大）  
 支持Line、Log、LogicLog、Duration数据轴  
 支持直方图、散点图、密度图、伪彩图、等高线图  
 支持段选门、双段段选门、椭圆门、多边形门、四边形门、横线门、竖线门、铰链门、十字门、逻辑门  
 支持逻辑计算通道  
 支持鼠标滚轮放大缩小，鼠标左键平移调整视场  
 支持门内数据突出显示及多图联动突出显示  
 支持随机抽样显示  
 支持撤销恢复  
 支持快速补偿  
 支持图中调整阈值  

 ![集成效果](doc/集成效果.png)

## FCSChart 流式细胞分析专用图表控件(已停止更新)

 Chart:不带门的显示控件  
 ChartWithGraphicals:带门绘制的显示控件  
 ChartInCollecting:带增益阈值调整功能的显示控件  
 轴类型有：线性（Line）、对数（Log10）  
 门类型有：多边形门（PolygonGraphical）、四边形门（RectangleGraphical）、段选门（SegmentXGraphical、SegmentYGraphical）、直线门（LineHorizontalGraphical、LineVerticalGraphical）、四象限门（QuadrantGraphical）

 [SDK文档](SDK.md)

 ![效果图](doc/charts.png)
