﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Test
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<FCSChart.Axis.IAxis> XAxisList = new List<FCSChart.Axis.IAxis>() { new FCSChart.Axis.LinearNumberAxis(), new FCSChart.Axis.LogarithmicNumberAxis(), new FCSChart.Axis.LogicleBiexpAxis() };
        List<FCSChart.Axis.IAxis> YAxisList = new List<FCSChart.Axis.IAxis>() { new FCSChart.Axis.LinearNumberAxis(), new FCSChart.Axis.LogarithmicNumberAxis(), new FCSChart.Axis.LogicleBiexpAxis() };
        public MainWindow()
        {
            InitializeComponent();
            this.xaxis.ItemsSource = XAxisList;
            this.yaxis.ItemsSource = YAxisList;
        }

        FCS.FCS Data;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "FCS文件|*.fcs";
            if (open.ShowDialog() == true)
            {
                var fcslist = FCS.Factory.ReadFile(open.FileName);
                if (fcslist.Count > 0)
                {
                    Data = fcslist[0];
                    xparams.ItemsSource = yparams.ItemsSource = Data.Measurements;
                    this.tbxMessage.Text = Data.TextSegment[FCS.Keys.TOTKey];
                }
            }
        }

        private void ButtonChartType_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button b && b.Content is string s)
            {
                switch (s)
                {
                    case "直方图":
                        c1.Series = new FCSChart.Series.HistogramSeries();
                        break;
                    case "散点图":
                        c1.Series = new FCSChart.Series.ScatterSeries();
                        break;
                    case "等高线图":
                        c1.Series = new FCSChart.Series.ContourSeries();
                        break;
                    case "密度图":
                        c1.Series = new FCSChart.Series.DensitySeries();
                        break;
                    default:
                        break;
                }
            }
        }

        private void xparams_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (xparams.SelectedItem is FCS.Property.Measurement m)
            {
                c1.XSource = m.Values;
            }
        }

        private void yparams_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (yparams.SelectedItem is FCS.Property.Measurement m)
            {
                c1.YSource = m.Values;
            }
        }

        private void xaxis_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (xaxis.SelectedItem is FCSChart.Axis.IAxis a)
            {
                c1.XAxis = a;
            }
        }

        private void yaxis_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (yaxis.SelectedItem is FCSChart.Axis.IAxis a)
            {
                c1.YAxis = a;
            }
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            c1.XAxis.ResetMaxMin(100000, 0);
            c1.YAxis.ResetMaxMin(100000, 0);
            //c1.Graphicals = new System.Collections.ObjectModel.ObservableCollection<FCSChart.Graphical.BaseGraphical>() { new FCSChart.Graphical.SegmentXGraphical(new FCSChart.Graphical.SegmentXGraphicalModel() { X1 = 50000, X2 = 70000, AreaNames = new string[] { "L1" } }) };
        }

        private void Button_HideGainClick(object sender, RoutedEventArgs e)
        {
            this.c1.ShowXGain = this.c1.ShowYGain = false;
        }
        private void Button_ShowGainClick(object sender, RoutedEventArgs e)
        {
            this.c1.ShowXGain = this.c1.ShowYGain = true;
        }
    }
}
