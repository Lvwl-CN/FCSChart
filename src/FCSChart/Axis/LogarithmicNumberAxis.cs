﻿using System;
using System.Collections.Generic;
using System.Windows;


namespace FCSChart.Axis
{
    /// <summary>
    /// 对数轴
    /// </summary>
    public class LogarithmicNumberAxis : IAxis
    {
        static LogarithmicNumberAxis()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(LogarithmicNumberAxis), new FrameworkPropertyMetadata(typeof(LogarithmicNumberAxis)));
        }

        public LogarithmicNumberAxis()
        {
            this.AxisName = "Log";
        }

        #region function
        /// <summary>
        /// 实际值转化成坐标值
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override double ValueToAxisValue(double value)
        {
            return Helper.Log10(value);
        }
        /// <summary>
        /// 坐标值转化成实际值
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override double AxisValueToValue(double axisvalue)
        {
            return Helper.Pow10(axisvalue);
        }
        /// <summary>
        /// 重新绘制坐标间隔
        /// </summary>
        internal override void Drawing()
        {
            if (!this.IsLoaded) return;
            var range = MaxAxis - MinAxis;
            var temp = MaxAxis % 1;
            var showedTempMax = temp > 0 ? Convert.ToInt32(MaxAxis - temp) + 1 : Convert.ToInt32(MaxAxis);
            temp = MinAxis % 1;
            var showedTempMin = temp > 0 ? Convert.ToInt32(MinAxis - temp) - 1 : Convert.ToInt32(MinAxis);
            double actualLength = XYType == AxisType.X ? this.ActualWidth : this.ActualHeight;
            List<LogarithmicNumbersScaleData> items = new List<LogarithmicNumbersScaleData>();
            for (int i = showedTempMin; i <= showedTempMax; i++)
            {
                var item = new LogarithmicNumbersScaleData() { Power = i < 0 ? -i : i, Value = i < 0 ? -10 : 10, Location = (i - MinAxis) * actualLength / range };
                if (item.Location >= 0 && item.Location <= actualLength) items.Add(item);
                var linelength = item.Length * 2 / 3;
                if (i != 0)
                {
                    for (int j = 1; j < 9; j++)
                    {
                        var m = ValueToAxisValue(AxisValueToValue(i) * (1 + j));
                        var mscale = new LogarithmicNumbersScaleData() { Location = (m - MinAxis) * actualLength / range, Length = linelength };
                        if (mscale.Location >= 0 && mscale.Location <= actualLength)
                            items.Add(mscale);
                    }
                }
                else
                {
                    for (int j = -9; j <= 9; j++)
                    {
                        var mscale = new LogarithmicNumbersScaleData() { Location = (j / 10d - MinAxis) * actualLength / range, Length = linelength };
                        if (mscale.Location >= 0 && mscale.Location <= actualLength)
                            items.Add(mscale);
                    }
                }
            }
            ItemsSource = items;
        }

        #endregion
    }

    internal class LogarithmicNumbersScaleData : IScaleData
    {
        private double? power = null;
        /// <summary>
        /// 幂
        /// </summary>
        public double? Power
        {
            get { return power; }
            set { power = value; OnPropertyChanged("Power"); }
        }

    }
}
