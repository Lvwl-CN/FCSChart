﻿using System;
using System.Collections.Generic;
using System.Windows;


namespace FCSChart.Axis
{
    /// <summary>
    /// 双指数对数轴
    /// </summary>
    public class LogicleBiexpAxis : IAxis
    {
        static LogicleBiexpAxis()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(LogicleBiexpAxis), new FrameworkPropertyMetadata(typeof(LogicleBiexpAxis)));
        }

        public LogicleBiexpAxis()
        {
            this.AxisName = "LopBiexp";
        }
        #region function
        /// <summary>
        /// 实际值转化成坐标值
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override double ValueToAxisValue(double value)
        {
            return Helper.LogBiExp(value);
        }
        /// <summary>
        /// 坐标值转化成实际值
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override double AxisValueToValue(double axisvalue)
        {
            return Helper.PowBiExp(axisvalue);
        }
        /// <summary>
        /// 重新绘制坐标间隔
        /// </summary>
        internal override void Drawing()
        {
            if (!this.IsLoaded) return;
            var range = MaxAxis - MinAxis;
            var tempMax = Helper.Log10(Max);
            var tempMin = Helper.Log10(Min);
            var temp = tempMax % 1;
            var showedTempMax = temp > 0 ? Convert.ToInt32(tempMax - temp) + 1 : Convert.ToInt32(tempMax);
            temp = tempMin % 1;
            var showedTempMin = temp > 0 ? Convert.ToInt32(tempMin - temp) - 1 : Convert.ToInt32(tempMin);
            double actualLength = XYType == AxisType.X ? this.ActualWidth : this.ActualHeight;
            List<LogicleBiexScaleData> items = new List<LogicleBiexScaleData>();
            for (int i = showedTempMin; i <= showedTempMax; i++)
            {
                var basev = Helper.Pow10(i);
                var item = new LogicleBiexScaleData()
                {
                    Power = i < 0 ? -i : i,
                    Value = i < 0 ? -10 : 10,
                    Location = (ValueToAxisValue(basev) - MinAxis) * actualLength / range
                };
                if (item.Power == 1)
                {
                    item.Power = null;
                    item.Value = null;
                }
                if (item.Location >= 0 && item.Location <= actualLength) items.Add(item);
                if (i != 0)
                {
                    var linelength = item.Length * 2 / 3;
                    for (int j = 1; j < 9; j++)
                    {
                        var mscale = new LogicleBiexScaleData() { Location = (ValueToAxisValue(basev * (1 + j)) - MinAxis) * actualLength / range, Length = linelength };
                        if (mscale.Location >= 0 && mscale.Location <= actualLength)
                            items.Add(mscale);
                    }
                }
            }
            ItemsSource = items;
        }

        #endregion
    }

    internal class LogicleBiexScaleData : IScaleData
    {
        private double? power = null;
        /// <summary>
        /// 幂
        /// </summary>
        public double? Power
        {
            get { return power; }
            set { power = value; OnPropertyChanged("Power"); }
        }

    }
}
