﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace FCSChart.Graphical
{
    /// <summary>
    /// 横线门
    /// </summary>
    public class LineHorizontalGraphical : BaseGraphical
    {
        private double y;

        public double Y
        {
            get { return y; }
            set { y = value; OnPropertyChanged("Y"); }
        }

        public LineHorizontalGraphical()
        {
            this.Fill = Brushes.Transparent;
        }
        public LineHorizontalGraphical(LineHorizontalGraphicalModel model) : base(model)
        {
            this.Y = model.Y;
            this.Fill = Brushes.Transparent;
            if (model.AreaNames != null && model.AreaNames.Length == 2)
            {
                Areas = model.AreaNames.Select(p => new GraphicalArea() { Name = p, OwnerGraphical = this }).ToArray();
                Helper.AddExistedGraphicalName(this.ShortName, model.AreaNames.ToArray());
                if (model.AreaColors != null && model.AreaColors.Length == 2)
                {
                    Areas[0].DisplayColor = model.AreaColors[0];
                    Areas[1].DisplayColor = model.AreaColors[1];
                }
            }
        }
        protected override void InitName()
        {
            this.Name = "LineHorizontal";
            this.ShortName = "LH";
        }
        internal override void Move(double x, double y) { }
        #region 绘制
        internal override void Drawing()
        {
            if (OwnerChart == null || !OwnerChart.IsLoaded
                || OwnerChart.XAxis == null || !OwnerChart.XAxis.IsLoaded || OwnerChart.XAxis.ActualWidth == 0
                || OwnerChart.YAxis == null || !OwnerChart.YAxis.IsLoaded || OwnerChart.XAxis.ActualHeight == 0) return;
            if (GraphicalShape == null)
            {
                Binding binding = new Binding("Y") { Source = this, Converter = Converters.LineHorizontalDoubleToGeometryConverter.Converter, ConverterParameter = OwnerChart, Mode = BindingMode.OneWay };
                var temp = new Path() { Cursor = Cursors.Hand };
                temp.SetBinding(Path.DataProperty, binding);
                temp.SetBinding(Shape.FillProperty, new Binding("Fill") { Source = this });
                temp.SetBinding(Shape.StrokeProperty, new Binding("Stroke") { Source = this });
                temp.SetBinding(Shape.StrokeThicknessProperty, new Binding("StrokeThickness") { Source = this });
                temp.SetBinding(FrameworkElement.ContextMenuProperty, new Binding("ContextMenu") { Source = this });
                temp.SetBinding(UIElement.FocusableProperty, new Binding("CanChangeGraphical") { Source = this.OwnerChart });
                temp.MouseDown += Graphical_MouseDown;
                GraphicalShape = temp;
            }
            OnPropertyChanged("Y");
            if (!IsCreateing) DrawingControl();
        }
        internal override void PanelMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (IsCreateing && sender is Panel panel)
            {
                var point = e.GetPosition(panel);
                Y = OwnerChart.YAxis.GetLocationValue(point.Y);
            }
        }

        internal override void PanelMouseMove(object sender, MouseEventArgs e)
        {
            if (IsCreateing && sender is Panel panel)
            {
                var point = e.GetPosition(panel);
                Y = OwnerChart.YAxis.GetLocationValue(point.Y);
            }
            else if (!IsCreateing && ControledShape != null)
            {
                Shape_MouseMove(ControledShape, e);
            }
        }

        internal override void PanelMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (IsCreateing)
            {
                IsCreateing = false;
            }
            else if (ControledShape != null)
            {
                Shape_MouseLeftButtonUp(ControledShape, e);
                ControledShape = null;
            }
        }
        #endregion

        #region 门控制
        protected override void DrawingControl()
        {
            if (GraphicalShape is Path graphical && graphical.Data is PathGeometry geometry && geometry.Figures.Count == 1 && geometry.Figures[0].Segments.Count == 1)
            {
                if (Areas == null) Areas = new GraphicalArea[] { new GraphicalArea() { Name = OwnerChart.CreateNewAreaNameFunction(this), OwnerGraphical = this }, new GraphicalArea() { Name = OwnerChart.CreateNewAreaNameFunction(this), OwnerGraphical = this } };
                this.OwnerChart.AddGraphicalArea(Areas[0], Areas[1]);

                var segment = geometry.Figures[0].Segments[0] as LineSegment;
                var point = new Point(segment.Point.X / 2, segment.Point.Y);
                if (ControlShapes.Count > 0)
                {
                    if (ControlShapes[0] is Path path && path.Data is EllipseGeometry ellipse)
                        ellipse.Center = point;
                }
                else
                {
                    ControlShapes.Add(new Path() { Data = new EllipseGeometry(point, 5, 5), Cursor = Cursors.SizeNS });
                }
                Areas[0].Center = new Point(point.X, point.Y / 2);
                Areas[1].Center = new Point(point.X, (OwnerChart.YAxis.ActualHeight + point.Y) / 2);
            }
        }
        protected override void Shape_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.OwnerChart.CanChangeGraphical && e.LeftButton == MouseButtonState.Pressed && sender is Path path && path.Data is EllipseGeometry ellipse)
            {
                var point = e.GetPosition(OwnerChart.ViewPanel);
                Y = OwnerChart.YAxis.GetLocationValue(point.Y);
                e.Handled = true;
                ellipse.Center = point;
            }
        }
        protected override void Shape_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.OwnerChart.CanChangeGraphical) DrawingControl();
            base.Shape_MouseLeftButtonUp(sender, e);
        }
        #endregion

        /// <summary>
        /// 更新门划分的区域内数据
        /// </summary>
        /// <param name="xValueConverter"></param>
        /// <param name="yValueConverter"></param>
        internal override async void RefreshAreaSource(Func<object, double> xValueConverter, Func<object, double> yValueConverter)
        {
            if (Areas == null || Areas.Length != 2) return;
            if (!isCreateing && OwnerChart != null && OwnerChart.YSource != null && OwnerChart.YAxis != null)
            {
                var ySource = OwnerChart.YSource;
                var count = ySource.Count;
                var parentIndexs = OwnerChart.Indexs;
                var target = Y;

                var maxDegreeOfParallelism = OwnerChart.Series == null ? 4 : OwnerChart.Series.MaxDegreeOfParallelism;
                try
                {
                    IsRefreshingAreaSource = true;
                    if (CancelTokenSource != null)
                    {
                        CancelTokenSource.Cancel();
                        CancelTokenSource.Dispose();
                    }
                    CancelTokenSource = new CancellationTokenSource();
                    var array = await Task.Factory.StartNew((tkn) =>
                    {
                        if (tkn is CancellationToken token)
                        {
                            ConcurrentBag<int>[] indexsArray = new ConcurrentBag<int>[] { new ConcurrentBag<int>(), new ConcurrentBag<int>() };
                            if (parentIndexs == null)
                            {
                                try
                                {
                                    var result = Parallel.For(0, count, new ParallelOptions() { CancellationToken = token, MaxDegreeOfParallelism = maxDegreeOfParallelism }, (i, loop) =>
                                    {
                                        if (loop.IsStopped) return;
                                        if (ySource.Count <= i || token.IsCancellationRequested) loop.Stop();
                                        else
                                        {
                                            var y = yValueConverter == null ? Convert.ToDouble(ySource[i]) : yValueConverter(ySource[i]);
                                            if (y > target) indexsArray[0].Add(i);
                                            else indexsArray[1].Add(i);
                                        }
                                    });
                                    if (!result.IsCompleted) return null;
                                }
                                catch (System.OperationCanceledException) { return null; }
                            }
                            else
                            {
                                try
                                {
                                    var result = Parallel.ForEach(parentIndexs, new ParallelOptions() { CancellationToken = token, MaxDegreeOfParallelism = maxDegreeOfParallelism }, (i, loop) =>
                                    {
                                        if (loop.IsStopped) return;
                                        if (ySource.Count <= i || token.IsCancellationRequested) loop.Stop();
                                        else
                                        {
                                            var y = yValueConverter == null ? Convert.ToDouble(ySource[i]) : yValueConverter(ySource[i]);
                                            if (y > target) indexsArray[0].Add(i);
                                            else indexsArray[1].Add(i);
                                        }
                                    });
                                    if (!result.IsCompleted) return null;
                                }
                                catch (System.OperationCanceledException) { return null; }
                            }
                            return indexsArray;
                        }
                        else return null;
                    }, CancelTokenSource.Token, CancelTokenSource.Token);

                    if (Areas == null || Areas.Length != 2 || array == null) return;
                    if (CancelTokenSource != null)
                    {
                        CancelTokenSource.Dispose();
                        CancelTokenSource = null;
                    }
                    this.Areas[0].InsideIndexs = array[0].ToArray();
                    this.Areas[1].InsideIndexs = array[1].ToArray();
                    IsRefreshingAreaSource = false;
                }
                catch (TaskCanceledException) { }
            }
            else
            {
                foreach (var area in Areas)
                {
                    area.InsideIndexs = null;
                }
            }
        }

        public override BaseGraphicalModel GetGraphicalMode()
        {
            if (IsCreateing) return null;
            return new LineHorizontalGraphicalModel()
            {
                AreaNames = this.Areas.Select(p => p.Name).ToArray(),
                AreaColors = this.Areas.Select(p => p.DisplayColor).ToArray(),
                Y = this.Y
            };
        }
    }

    public class LineHorizontalGraphicalModel : BaseGraphicalModel
    {
        public double Y { get; set; }

    }
}
