﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace FCSChart.Converters
{
    /// <summary>
    /// double 相乘
    /// </summary>
    internal class DoubleMultiplyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double d && parameter is double m) return d * m;
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double d && parameter is double m) return d / m;
            return value;
        }
    }
}
