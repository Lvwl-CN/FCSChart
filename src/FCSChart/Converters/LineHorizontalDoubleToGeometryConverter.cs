﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace FCSChart.Converters
{
    /// <summary>
    /// 横线门模型数据转化为图形
    /// </summary>
    internal class LineHorizontalDoubleToGeometryConverter : IValueConverter
    {
        internal static readonly LineHorizontalDoubleToGeometryConverter Converter = new LineHorizontalDoubleToGeometryConverter();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double temp && parameter is Chart chart)
            {
                var length = chart.XAxis.ActualWidth;
                var y = chart.YAxis.GetValueLocation(temp);
                PathFigure figure = new PathFigure()
                {
                    StartPoint = new Point(0, y),
                    Segments = new PathSegmentCollection()
                    {
                        new LineSegment(new Point(length,y), true)
                    }
                };
                PathGeometry geometry = new PathGeometry() { FillRule = FillRule.Nonzero, Figures = new PathFigureCollection() { figure } };
                geometry.Freeze();
                return geometry;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
