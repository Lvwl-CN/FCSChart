﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace FCSChart.Converters
{
    /// <summary>
    /// axis坐标值点位转化为图形显示位置值
    /// </summary>
    internal class AxisValueToViewValueConverter : IValueConverter
    {
        /// <summary>
        /// 点位坐标值转化实际显示位置
        /// </summary>
        internal static readonly AxisValueToViewValueConverter Converter = new AxisValueToViewValueConverter();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Point point && parameter is Chart chart)
            {
                var xvalue = chart.XAxis.GetValueLocation(point.X);
                var yvalue = chart.YAxis.GetValueLocation(point.Y);
                return new Point(xvalue, yvalue);

            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Point point && parameter is Chart chart)
            {
                var xvalue = chart.XAxis.GetLocationValue(point.X);
                var yvalue = chart.YAxis.GetLocationValue(point.Y);
                return new Point(xvalue, yvalue);
            }
            return null;
        }
    }
}
