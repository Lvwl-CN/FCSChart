﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace FCSChart.Converters
{
    /// <summary>
    /// 中心点位置转文本框在容器中的位置
    /// </summary>
    internal class TextBlockCenterPointToThicknessConverter : IValueConverter
    {
        internal static readonly TextBlockCenterPointToThicknessConverter Converter = new TextBlockCenterPointToThicknessConverter();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Point p && parameter is TextBlock element)
            {
                return new Thickness(p.X - element.FontSize * element.Text.Length / 2d, p.Y - element.FontSize / 2, 0, 0);
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
