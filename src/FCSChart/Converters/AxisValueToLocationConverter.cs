﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace FCSChart.Converters
{
    /// <summary>
    /// 实际值转轴位置
    /// </summary>
    internal class AxisValueToLocationConverter : IValueConverter
    {
        internal static readonly AxisValueToLocationConverter Converter = new AxisValueToLocationConverter();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double v && parameter is Axis.IAxis axis)
            {
                return axis.GetValueLocation(v);
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double v && parameter is Axis.IAxis axis)
            {
                return axis.GetLocationValue(v);
            }
            return null;
        }
    }
}
