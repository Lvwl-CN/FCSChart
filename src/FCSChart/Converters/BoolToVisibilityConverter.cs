﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace FCSChart.Converters
{
    internal class BoolToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// bool转化为visibility值
        /// </summary>
        internal static readonly BoolToVisibilityConverter Converter = new BoolToVisibilityConverter();
        /// <summary>
        /// 是否反转
        /// </summary>
        public bool IsReverse { get; set; } = false;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool b) return (IsReverse ? !b : b) ? Visibility.Visible : Visibility.Collapsed;
            return IsReverse ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility v) return IsReverse ? v != Visibility.Visible : v == Visibility.Visible;
            return IsReverse;
        }
    }
}
