﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace FCSChart.Converters
{
    /// <summary>
    /// 竖线门模型数据转化为图形
    /// </summary>
    internal class LineVerticalDoubleToGeometryConverter : IValueConverter
    {
        internal static readonly LineVerticalDoubleToGeometryConverter Converter = new LineVerticalDoubleToGeometryConverter();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double temp && parameter is Chart chart)
            {
                var length = chart.YAxis.ActualHeight;
                var x = chart.XAxis.GetValueLocation(temp);
                PathFigure figure = new PathFigure()
                {
                    StartPoint = new Point(x, 0),
                    Segments = new PathSegmentCollection()
                    {
                        new LineSegment(new Point(x,length), true)
                    }
                };
                PathGeometry geometry = new PathGeometry() { FillRule = FillRule.Nonzero, Figures = new PathFigureCollection() { figure } };
                geometry.Freeze();
                return geometry;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
