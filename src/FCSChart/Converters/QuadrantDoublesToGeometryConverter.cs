﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace FCSChart.Converters
{
    /// <summary>
    /// 4象限点位转图形数据
    /// </summary>
    internal class QuadrantDoublesToGeometryConverter : IValueConverter
    {
        internal static readonly QuadrantDoublesToGeometryConverter Converter = new QuadrantDoublesToGeometryConverter();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is IList<double> points && points != null && points.Count == 6 && parameter is Chart chart)
            {
                var center = new Point(chart.XAxis.GetValueLocation(points[4]), chart.YAxis.GetValueLocation(points[5]));
                var left = new Point(0, chart.YAxis.GetValueLocation(points[0]));
                var top = new Point(chart.XAxis.GetValueLocation(points[1]), 0);
                var right = new Point(chart.ViewPanel.ActualWidth, chart.YAxis.GetValueLocation(points[2]));
                var bottom = new Point(chart.XAxis.GetValueLocation(points[3]), chart.ViewPanel.ActualHeight);
                StreamGeometry geometry = new StreamGeometry();
                using (StreamGeometryContext sgc = geometry.Open())
                {
                    sgc.BeginFigure(left, true, false);
                    sgc.LineTo(center, true, false);
                    sgc.LineTo(right, true, false);
                    sgc.BeginFigure(top, true, false);
                    sgc.LineTo(center, true, false);
                    sgc.LineTo(bottom, true, false);
                }
                geometry.Freeze();
                return geometry;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
