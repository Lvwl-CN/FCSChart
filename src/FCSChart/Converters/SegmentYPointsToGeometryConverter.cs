﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace FCSChart.Converters
{
    /// <summary>
    /// Y轴段选门模型数据转化为图形
    /// </summary>
    internal class SegmentYPointsToGeometryConverter : IValueConverter
    {
        internal static readonly SegmentYPointsToGeometryConverter Converter = new SegmentYPointsToGeometryConverter();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Point temp && parameter is Chart chart)
            {
                var length = chart.XAxis.ActualWidth;
                var y1 = chart.YAxis.GetValueLocation(temp.X);
                var y2 = chart.YAxis.GetValueLocation(temp.Y);
                PathFigure figure = new PathFigure()
                {
                    StartPoint = new Point(0, y1),
                    Segments = new PathSegmentCollection()
                    {
                        new LineSegment(new Point(length,y1), true),
                        new LineSegment(new Point(length,y2), false),
                        new LineSegment(new Point(0,y2), true),
                        new LineSegment(new Point(0,y1), false)
                    }
                };
                PathGeometry geometry = new PathGeometry() { FillRule = FillRule.Nonzero, Figures = new PathFigureCollection() { figure } };
                geometry.Freeze();
                return geometry;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
