﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace FCSChart.Converters
{
    /// <summary>
    /// X轴段选门模型数据转化为图形
    /// </summary>
    internal class SegmentXPointsToGeometryConverter : IValueConverter
    {
        internal static readonly SegmentXPointsToGeometryConverter Converter = new SegmentXPointsToGeometryConverter();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Point temp && parameter is Chart chart)
            {
                var length = chart.YAxis.ActualHeight;
                var x1 = chart.XAxis.GetValueLocation(temp.X);
                var x2 = chart.XAxis.GetValueLocation(temp.Y);
                PathFigure figure = new PathFigure()
                {
                    StartPoint = new Point(x1, 0),
                    Segments = new PathSegmentCollection()
                    {
                        new LineSegment(new Point(x1, length), true),
                        new LineSegment(new Point(x2, length), false),
                        new LineSegment(new Point(x2, 0), true),
                        new LineSegment(new Point(x1, 0), false)
                    }
                };
                PathGeometry geometry = new PathGeometry() { FillRule = FillRule.Nonzero, Figures = new PathFigureCollection() { figure } };
                geometry.Freeze();
                return geometry;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
