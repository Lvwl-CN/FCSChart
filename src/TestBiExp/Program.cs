﻿using System;

namespace TestBiExp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                var s = Console.ReadLine();
                if ("exit".Equals(s.ToLower())) break;
                else if ("clear".Equals(s.ToLower())) Console.Clear();
                else if (double.TryParse(s, out double d))
                {
                    var temp1 = FCSChart.Helper.LogBiExp(d);
                    Console.WriteLine(String.Format("输入{0}，正向公式计算结果为{1},再反向公式计算为{2}", d, temp1, FCSChart.Helper.PowBiExp(temp1)));
                    var temp2 = FCSChart.Helper.PowBiExp(d);
                    Console.WriteLine(String.Format("输入{0}，反向公式计算结果为{1},再正向公式计算为{2}", d, temp2, FCSChart.Helper.LogBiExp(temp2)));
                }
            }
        }
    }
}
