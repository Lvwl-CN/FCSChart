## 文件目录结构
| 名称 | 说明 |
| --- | --- |
| Axis | XY轴上的分隔刻度控件 |
| Converters | IValueConverter的实现类，用于值的转换，方便在WPF界面上显示 |
| Graphical | 门 |
| Series | 主要的图表数据显示控件，图表中心区域显示的内容由此目录下的类实现 |
| Themes | WPF样式模板等资源文件 |
| Chart | 图表控件基类 |
| ChartWithGraphicals | 带门绘制的图表控件 |
| ChartInCollecting | 带阈值和增益调整功能的图表控件 |
| DelegateCommand | ICommand实现类 |
| Helper | 帮助类，一些静态方法，包括随机颜色值等 |
| NotifyPropertyChanged | INotifyPropertyChanged实现类 |

## Chart
### Chart图表控件主要属性
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| XSource | X轴数据源 | IList | 依赖属性。IList里的数据必须为可转成double的数据 |
| YSource | Y轴数据源 | IList | 依赖属性。IList里的数据必须为可转成double的数据 |
| Indexs | 数据源的过滤集合 | IList< int > | 依赖属性。数据源中的数据，索引在此集合的才显示。为空时表示使用数据源的所有值 |
| XValueConverter | XSource值转换方法 | Func< object, double > | 依赖属性。FCS文件里面的数据源是通道值，需要转换成刻度值 |
| YValueConverter | YSource值转换方法 | Func< object, double > | 依赖属性。FCS文件里面的数据源是通道值，需要转换成刻度值 |
| Series | 图形绘制类型 | ISeries | 依赖属性。ISeries类的子类对象，ContourSeries（等高线图）、DensitySeries（密度图）、HistogramSeries（直方图）、ScatterSeries（散点图） |
| XAxis | X轴类型 | IAxis | 依赖属性。IAxis类的子类对象，可选 LinearNumberAxis(线性)、LogarithmicNumberAxis（对数）、LogicleBiexAxis（可负数的对数） |
| YAxis | Y轴类型 | IAxis | 依赖属性。 IAxis类的子类对象，可选 LinearNumberAxis(线性)、LogarithmicNumberAxis（对数）、LogicleBiexAxis（可负数的对数）|
| ZoomType | 缩放方式 | AxisChangeType | 可指定鼠标滚轮缩放时，缩放X轴、Y轴或者两者都缩放或者都不缩放 |
| MoveType | 移动方式 | AxisChangeType | 可指定鼠标左键在图表显示区域按下并移动时，X轴、Y轴或者两者或者都不跟随鼠标改变最大最小值 |
 

 ### ChartWithGraphicals图表控件主要属性和方法(继承自Chart)
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| CanRemoveGraphical | 是否可以删除门，用于门ContentMenu显示 | bool | 默认true |
| CanChangeGraphical | 是否可以编辑门，控制门是否可以移动和调整点位 | bool | 默认true |
| Graphicals | 门集合 | ObservableCollection< BaseGraphical > | 该图表内所绘制的门的集合 |
| AllAreas | 门划分的区域集合 | ObservableCollection< GraphicalArea > | 添加门后，门划分的区域会在此集合内。多图的集合共用同一个对象时，散点图和直方图会以不同颜色显示所有门的区域 |
| AreaNameForeground | 区域名称显示的文字颜色 | Brush | 图表上区域内会显示一个名称 |
| AreaNameFontSize | 区域名称显示的字体大小 | double | 图表上区域内会显示一个名称 |
| GraphicalAreaAddCommand | 区域添加命令 | ICommand | 添加一个区域后会执行此命令 |
| GraphicalAreaRemoveCommand | 区域删除命令 | ICommand | 删除一个区域后会执行此命令 |
| PolygonCommand | 画多边形门命令 | ICommand | 依赖属性 |
| RectangleCommand | 画四边形门命令 | ICommand | 依赖属性 |
| SegmentXCommand | 画X轴段选门命令 | ICommand | 依赖属性 |
| SegmentYCommand | 画Y轴段选门命令 | ICommand | 依赖属性 |
| QuadrantCommand | 画四象限门命令 | ICommand | 依赖属性 |
| LineHorizontalCommand | 画横线门命令 | ICommand | 依赖属性 |
| LineVerticalCommand | 画竖线门命令 | ICommand | 依赖属性 |
| AddGraphicalFromModel（BaseGraphicalModel model） | 向图表添加一个门 | void | 根据不同的model类型添加不同类型的门 |
 
### ChartInCollecting图表控件主要属性和方法(继承自ChartWithGraphicals)
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| CanChangeGain | 是否可以编辑增益 | bool | 默认true |
| MinGain | 增益最小值 | double | 默认0 |
| MaxGain | 增益最大值 | double | 默认4096 |
| ShowXGain | 是否显示x轴上的增益 | bool | 默认false |
| ShowYGain | 是否显示y轴上的增益 | bool | 默认false |
| XGainValue | x轴的增益值 | double | 控制方式是移动x轴上的Thumb控件，鼠标移动到图表控件上时可见 |
| YGainValue | y轴的增益值 | double | 控制方式是移动y轴上的Thumb控件，鼠标移动到图表控件上时可见 |
| XGainChangedCommand | x轴上的增益更改完成命令 | ICommand |  |
| YGainChangedCommand | y轴上的增益更改完成命令 | ICommand |  |
| MinThreshold | 阈值最小值 | double | 默认0 |
| MaxThreshold | 阈值最大值 | double | 默认65535 |
| ShowXThreshold | 显示x轴上的阈值 | bool | 默认false |
| ShowYThreshold | 显示y轴上的阈值 | bool | 默认false |
| XThresholdValue | x轴上的阈值 | double | 控制方式是移动图中垂直于x轴的Line控件 |
| YThresholdValue | y轴上的阈值 | double | 控制方式是移动图中垂直于y轴的Line控件 |
| XThresholdChangedCommand | x轴阈值发生变化命令 | ICommand |  |
| YThresholdChangedCommand | y轴阈值发生变化命令 | ICommand |  |

## Axis 轴
### IAxis 轴基类
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| AxisName | 轴类型名称 | string | Line,Log,LopBiexp |
| Max | 最大值 | double | 只读 |
| Min | 最小值 | double | 只读 |
| MaxAxis | 转化后的最大值 | double | 只读 |
| MinAxis | 转化后的最小值 | double | 只读 |
| ResetMaxMin(double max, double min) | 重置最大最小值 | void |  |
| Redrawing() | 手动刷新一下 | void | 没啥大用，基本用不到 |
| ValueToAxisValue(double value) | 实际值转换成坐标值 | double |  |
| AxisValueToValue(double axisvalue) | 坐标值转换成实际值 | double |  |
| PointAxisValue(Point point) | 鼠标点位的坐标值 | double |  |
| PointValue(Point point) | 鼠标点位的实际值 | double |  |
| GetConvertParam() | 获取实际值转化成坐标值的参数 | ValueLocationConvertParam | 下面几个方法中用到 |
| GetValueLocation(double value)、GetValueLocation(double value, ValueLocationConvertParam convert) | 获取真实数据对应的图中位置 | double |  |
| GetAxisValueLocation(double value)、GetAxisValueLocation(double value, ValueLocationConvertParam convert) | 获取坐标数字对应的图中位置 | double |  |
| GetLocationAxisValue(double value)、GetLocationAxisValue(double value, ValueLocationConvertParam convert) | 获取点位对应的坐标值 | double |  |
| GetLocationValue(double value)、GetLocationValue(double value, ValueLocationConvertParam convert) | 获取点位对应的实际值 | double |  |

### LogarithmicNumberAxis 对数轴（继承自IAxis）
### LinearNumberAxis 线性轴（继承自IAxis）
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| Power | 10的次方数 | int | 过大的数字以 n *10次方数表示 |


## Series 显示图形
### ISeries 图形基类
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| SeriesName | 控件名称 | string |  |
| Fill | 填充色 | Color | 散点图、直方图等内填充色 |
| Stroke | 边框色 | Color | 图中线的基本颜色 |
| Tickness | 边框线宽 | double | 图中线的宽度 |
| MaxDegreeOfParallelism | 生成适量图形数据时启用的最大线程数 | int | 默认4 |
| OwnerChart | 所属的Chart控件 | Chart | 在本对象赋值给Chart的Series属性后，该属性会有值 |
| GetGradientColor | 获取渐变颜色的方法 | Func< long, long, Color, Color > | 等高线图的线的颜色和密度图的点的颜色由该方法生成 |

### HistogramSeries直方图(继承自ISeries)
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| Count | 直方图等份数量 | int | x轴的平分等份数量，默认512 |
| Smooth | 是否启用平滑处理 | bool | 默认false |
| SmoothPointCount | 简单平滑算法的点数 | int | 默认6 |

### ScatterSeries散点图(继承自ISeries)
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| PointLength | 散点的点的宽度 | int | 默认1 |

### DensitySeries密度图(继承自ISeries)
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| PointLength | 散点的点的宽度 | int | 默认1 |
| GradesBase | 密度梯度 | byte | 以此为底，每加一次方，对应颜色会改变一次。默认4 |

### ContourSeries等高线图(继承自ISeries)
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| PointLength | 散点的点的宽度 | int | 默认1 |
| GradesBase | 密度梯度 | byte | 以此为底，每加一次方，对应颜色会改变一次。默认4 |
| AreaLength | 小区域的长宽 | byte | 区域内的数量为画线的基础。Marching Squares算法 |

## Graphical 门
### BaseGraphical 门基类
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| Name | 门类型名称 | string | 只读 |
| ShortName | 门类型简称 | string | 门内区域命名时使用，只读 |
| OwnerChart | 所属父图 | ChartWithGraphicals | 只读 |
| IsCreateing | 是否正在创建中 | bool | 新增的门需要在OwnerChart中绘制完成，只读 |
| Areas | 门内的区域集合 | GraphicalArea[] | 门未创建完成的情况下为null，只读 |
| ContextMenu | 门的内容菜单 | ContextMenu | 默认会有删除按钮 |
| Fill | 门的填充色 | Brush | 封闭区域的门才会使用，默认不透明度为0x01，方便选中移动门 |
| Stroke | 门的边框色 | Brush | 默认黑色 |
| StrokeThickness | 门的边框宽度 | double | 默认1 |
| IsRefreshingAreaSource | 是否正在刷新门内区域的数据 | bool | 正在计算门内区域的数据时为true，只读 |
| GetGraphicalMode() | 获取门的数据模式 | BaseGraphicalModel | 实例化门时，构造函数可使用此方法生成的数据模型 |
| Dispose() | 删除门并清空门相关的所有资源 | void | 删除门时可使用 |
| GraphicalModelsToGraphical(params BaseGraphicalModel[] models) | 门的数据模型批量转换为门对象 | List< BaseGraphical > | 静态方法 |
| CreateFromGraphicalName(string graphicalName) | 创建指定类型的门 | BaseGraphical |  |

#### GraphicalArea 门划分的区域
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| Name | 区域名称 | string |  |
| Center | 区域中心点位 | Point | 图中的位置，无需赋值，控件会自动计算 |
| InsideIndexs | 区域内数据的索引集合 | IList< int > |  |
| DisplayColor | 区域内图的显示颜色 | Color | 会自动生成不透明颜色 |
| OwnerGraphical | 所属门 | BaseGraphical | 只读 |

### LineHorizontalGraphical 横线门(继承自BaseGraphical)-将图分为上下两个区域
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| Y | 横线的y轴坐标值 | double |  |

### LineVerticalGraphical 竖线门(继承自BaseGraphical)-将图分为左右两个区域
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| X | 竖线的x轴坐标值 | double |  |

### PolygonGraphical 多边形门(继承自BaseGraphical)
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| Points | 多边形点位 | List< Point > |  |

### RectangleGraphical 四边形门(继承自PolygonGraphical)

### QuadrantGraphical 四象限门(继承自BaseGraphical)
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| Points | 四象限门的五个点的位置 | double[ 6 ] | 前四个点都在图的边框上，所以只需要记录一个值。1：左边点的y值；2：上面点的x值；3：右边点的y值；4：下面点的x值；5：中心点的x值；6：中心点的y值； |

### SegmentXGraphical x轴段选门(继承自BaseGraphical)
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| X12 | 段选门在x轴上的位置 | Point? | point的xy值对应段选门左右两条线的x轴位置值 |

### SegmentYGraphical y轴段选门(继承自BaseGraphical)
| 属性 | 含义 | 类型 | 说明 |
| --- | --- | --- | --- |
| Y12 | 段选门在y轴上的位置 | Point? | point的xy值对应段选门上下两条线的y轴位置值 |

## 示例
### 前端
```
        <chart:ChartWithGraphicals x:Name="c1">
            <chart:Chart.Series>
                <series:ContourSeries/>
            </chart:Chart.Series>
            <chart:Chart.XAxis>
                <axis:LinearNumberAxis/>
            </chart:Chart.XAxis>
            <chart:Chart.YAxis>
                <axis:LinearNumberAxis/>
            </chart:Chart.YAxis>
        </chart:ChartWithGraphicals>
```
### 后端
使用到FCS库
```
            var fcss = FCS.Factory.ReadFCSFile(@"C:\test.fcs").ToList();
            if (fcss.Count > 0) 
            {
                    var fcs = fcslist[0];
                    c1.XSource = fcs.Measurements[0].Values;
                    c1.YSource = fcs.Measurements[1].Values;
            }
```
